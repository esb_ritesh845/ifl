import {BrowserRouter as Router, Switch,Route} from "react-router-dom";
import React from 'react';
import LoginSignupContainer from './components/LoginSignup/LoginSignup.container';
import HomeContainer from "./components/Home/Home.container";
import ApplyOnlineContainer from './components/ApplyOnline/ApplyOnline.container';
import ApplyOnlineFormContainer from './components/ApplyOnline/ApplyOnlineForm.container';
import ApplyNowContainer from "./components/ApplyNow/ApplyNowContainer";
class App extends React.Component {
    render(){
        return (
            <>
                <Router>
                    <Switch>
                        <Route path="/apply-online-form">
                            <ApplyOnlineFormContainer />
                        </Route>
                        <Route path="/apply-online">
                            <ApplyOnlineContainer />
                        </Route>
                        <Route path="/apply-now">
                            <ApplyNowContainer />
                        </Route>
                        <Route path="/login-or-signup">
                            <LoginSignupContainer />
                        </Route>
                        <Route exact path="/">
                            <HomeContainer />
                        </Route>
                    </Switch>
                </Router>
            </>
        );
    }
}

export default App;
