import {PureComponent} from 'react';
import {connect} from 'react-redux';
import HomeComponent from './Home.component';
import PropTypes from 'prop-types';
import {saveToken} from '../../store/loginDetails/loginDetail.action'

const mapStateToProps=state=>({
    userDetails:state.LoginDetailsReducer.userDetails
})
const mapDispatchToProps=dispatch=>({
    saveToken: state => dispatch(saveToken(state)),
})

class HomeContainer extends PureComponent{

    static propTypes = {
        userDetails:PropTypes.any.isRequired,
        saveToken:PropTypes.func.isRequired
    }

    render(){

        return (
            <>
                 <HomeComponent {...this.props}/>
            </>
        );
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(HomeContainer)
