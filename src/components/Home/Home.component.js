import {PureComponent} from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Footer from '../Footer';
import NavBarContainer from '../NavBar/NavBar.container';


class Home extends PureComponent {

    static propTypes = {
        userDetails:PropTypes.any.isRequired,
        saveToken:PropTypes.func.isRequired
    }

    renderNavBarComponent(){

        return <NavBarContainer />;
    }
    renderFooterComponent(){
        return <Footer />;
    }
    renderContent(){

        return <div className="container-fluid bg-img bg-white" >
                <div className="row">
                    <div className="col-md-6 tbl-midd">
                    </div>
                    <div className="col-md-6 tbl-midd">
                        <div className="text-div cont-bottom">
                            <div className="head_main_text"> Home Loans At The Best Rates </div>
                            <div className="head_sub_text"> Quick - Simple - Transparent </div>
                            <div className="btn-div">
                                <Link to="/login-or-signup" className="btn-side" > DSA </Link>
                                <Link to="/" className="btn-side ml-4">Loan Customer </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    }

    render(){
        return (
            <>
                {this.renderNavBarComponent()}
                {this.renderContent()}
                {this.renderFooterComponent()}
            </>
        );
    }
  }

  export default Home;
