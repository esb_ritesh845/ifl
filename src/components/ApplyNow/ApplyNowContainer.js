import {PureComponent} from 'react';
import {connect} from 'react-redux';
import ApplyNowComponent from './ApplyNow.component';
import PropTypes from 'prop-types';
import {changeOtpStatus} from '../../store/loginDetails/loginDetail.action'

const mapStateToProps = state => ({
    otpStatus:state.LoginDetailsReducer.otpStatus,
})
const mapDispatchToProps = dispatch => ({
    changeOtpStatus: state => dispatch(changeOtpStatus(state))
})

class ApplyNowContainer extends PureComponent{
    static propTypes = {
        changeOtpStatus:PropTypes.func.isRequired
    }
    render(){
        return(
            <>
                <ApplyNowComponent {...this.props} />
            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ApplyNowContainer)
