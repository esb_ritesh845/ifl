import {PureComponent} from 'react';
import OtpContainer from '../Otp/Otp.container';
import Footer from '../Footer';
import NavBarContainer from '../NavBar/NavBar.container';
class ApplyNowComponent extends PureComponent {
    renderNavBarComponent(){
        return <NavBarContainer />;
    }
    renderFooterComponent(){
        return <Footer />;
    }

    renderFormContent(){
        const{changeOtpStatus} = this.props;
        return <div className="tab-pane active signup_section" id="home" role="tabpanel">
            <form action="" method="post" id="" >
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label className="input-lbl m-font17"> Name </label>
                            <input type="text" className="form-control" name="full_name" defaultValue=""
                                placeholder=" Enter Name" autoComplete="off" />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label className="input-lbl m-font17"> Father Name </label>
                            <input type="text" className="form-control" name="full_name" defaultValue=""
                                placeholder=" Enter Father Name" autoComplete="off" />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="form-group col-lg-12 mt-4">
                        <label className="input-lbl m-font17 float-left">Mobile number</label>
                        <span className="clearfix"></span>
                        <select name="mobile_country_code" className="w31" aria-invalid="false">
                            <option defaultValue="+91">+91</option>
                        </select>
                        <input type="text" className="signup-mobile" name="mobile_no" placeholder="Enter your Mobile No." min="0" defaultValue="" autoComplete="off" />
                        <div className="otp_note_text">You will receive the OTP on this mobile number.</div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12 form-group mt-3">
                        <div className="custom-control custom-checkbox">
                            <input type="checkbox" name="agree" className="custom-control-input" id="defaultUnchecked" />
                            <label className="custom-control-label font14" htmlFor="defaultUnchecked"> I/We declare that all the
                                particulars and information given/provided by me/us are true, correct, accurate and complete and
                                I/We understand that they shall form the basis of any loan IFL may decide to grant me/us. I/We
                                hereby authorize IFL & its representatives to call or SMS with reference to my "IFL Online"
                                application, notwithstanding anything to the contrary that may be contained anywhere, including
                                any registration for DNC/ NDNC.
                            </label>
                        </div>
                    </div>
                </div>
                <div className="row mb-4">
                    <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-40 mb-20">

                        <button className="btn-common" type="submit" onClick={(() => changeOtpStatus(true))}> SUBMIT</button>
                    </div>
                </div>
            </form>
        </div>
    }

    renderContent(){
        const{otpStatus} = this.props;
        return   <div className="container-fluid bg-white singup_bg">
                    <div className="signup_card">
                        <div className="row margin0">
                            <div className="col-lg-8">
                                <div className="signup_box">
                                    <div className="login-head"> Apply Now</div>
                                </div>

                                <div className="row signup-tab tab-signup-div">
                                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                                        <li className="nav-item">
                                            <a className={"nav-link active"}  id="withaccount" data-toggle="tab" role="tab"
                                            aria-controls="home" >
                                                <div><input id="radio-1" className="radio-custom" name="radio-group" type="radio"
                                                            defaultChecked="true"
                                                            />
                                                <label htmlFor="radio-1" className="radio-custom-label"> </label></div>
                                                Apply Now
                                            </a>
                                        </li>

                                    </ul>
                                    <div className="tab-content">

                                        {otpStatus ? <OtpContainer /> : this.renderFormContent() }

                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4"><img className="signup_img" src="../images/signup_pic.png" /></div>
                        </div>
                    </div>
                </div>
    }
    render(){
        return (
            <>
                {this.renderNavBarComponent()}
                {this.renderContent()}
                {this.renderFooterComponent()}
            </>
        )
    }
}

export default ApplyNowComponent;
