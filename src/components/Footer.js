import {PureComponent} from 'react';

class Footer extends PureComponent{
    render(){
        return(
            <>
              <footer class="footer mob-hide">
                <div class="container-flex">
                    <div class="row m-0">
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 text-left"> Copyright © 2021 IFL All rights reserved.
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 text-right hidden-xs">
                            <a href="" target="_blank" rel="noopener noreferrer"> User Agreement </a>
                            &nbsp;&nbsp; | &nbsp;&nbsp;
                            <a href="" target="_blank" rel="noopener noreferrer"> Privacy Policy </a>
                            &nbsp;&nbsp; | &nbsp;&nbsp;
                            <a href="" target="_blank" rel="noopener noreferrer"> Disclaimer </a>
                            &nbsp;&nbsp; | &nbsp;&nbsp;
                            <a href="" target="_blank" rel="noopener noreferrer"> Contact Us </a></div>
                    </div>
                </div>
            </footer>
            </>
        )
    }
}
export default Footer;
