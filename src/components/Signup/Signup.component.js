import {PureComponent} from 'react';

class Signup extends PureComponent {
    renderContent(){

        const {changeOtpStatus} = this.props;
        return <div className="tab-pane active signup_section" id="home" role="tabpanel">
                <form action="https://portal.IFL.com/signup" method="post" id="signup-form" >
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="input-lbl m-font17"> Name </label>
                                <input type="text" className="form-control" name="full_name" defaultValue=""
                                    placeholder=" Enter Name" autoComplete="off" />
                            </div>
                        </div>
                        <div className="col-lg-6 ">
                            <div className="form-group">
                                <label className="input-lbl m-font17"> User ID </label>
                                <input type="text" className="form-control" name="user_id" id="signup_user_id" placeholder="Create Your User ID"
                                    defaultValue=""
                                    autoComplete="off" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 mt-4">
                            <div className="form-group">
                                <label className="input-lbl m-font17"> PAN Number </label>
                                <input type="text" className="form-control" name="pan_no" placeholder=" Enter Your PAN Number"
                                    defaultValue=""  autoComplete="off" />
                            </div>
                        </div>

                    </div>
                    <div className="row">
                        <div className="col-lg-6 mt-4">
                            <div className="form-group">
                                <label className="input-lbl m-font17"> Password </label>
                                <input type="password" className="form-control" name="password" id="signup_password" placeholder=" Enter Password" defaultValue=""
                                    autoComplete="off" />
                            </div>
                        </div>
                        <div className="col-lg-6 mt-4">
                            <div className="form-group">
                                <label className="input-lbl m-font17"> Confirm Password </label>
                                <input type="password" className="form-control" name="password_confirmation"
                                    placeholder=" Enter Confirm Password"
                                    defaultValue="" autoComplete="off" />
                            </div>
                        </div>
                        <div className="row mt-4 margin0">
                            <div className="col-lg-12">
                                <span className="note_blue"> Note: Minimum 8 characters are required, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-lg-12 mt-4">
                            <label className="input-lbl m-font17 float-left">Mobile number</label>
                            <span className="clearfix"></span>
                            <select name="mobile_country_code" className="w31" aria-invalid="false">
                                <option defaultValue="+91">+91</option>
                            </select>
                            <input type="text" className="signup-mobile" name="mobile_no" placeholder="Enter your Mobile No." min="0" defaultValue="" autoComplete="off" />
                            <div className="otp_note_text">You will receive the OTP on this mobile number.</div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-12">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" name="agree" className="custom-control-input" id="defaultUnchecked" />
                                <label className="custom-control-label font14" htmlFor="defaultUnchecked"> I/We declare that all the
                                    particulars and information given/provided by me/us are true, correct, accurate and complete and
                                    I/We understand that they shall form the basis of any loan IFL may decide to grant me/us. I/We
                                    hereby authorize IFL & its representatives to call or SMS with reference to my "IFL Online"
                                    application, notwithstanding anything to the contrary that may be contained anywhere, including
                                    any registration for DNC/ NDNC.
                                </label>
                            </div>
                        </div>
                    </div>

                    <div className="row mb-4">
                        <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-40 mb-20">

                            <button className="btn-common" type="submit" onClick={(() => changeOtpStatus(true))}> SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>
    }
    render(){
        return (
            <>
                {this.renderContent()}
            </>
        )
    }
}
export default Signup;
