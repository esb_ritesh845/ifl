import {PureComponent} from 'react';

class OtpComponent extends PureComponent {
    renderContent(){
        return <div class="otp_section" >
                    <form id="SignUpfrmVerifyOtp"  class="tab_w50 row"  name="SignUpfrmVerifyOtp"  method="post"
                        action="">
                        <div class="w60">

                        <div class="form-group col-lg-12 col-sm-12 col-md-12 margin">
                            <label class="input-lbl m-font17">Enter OTP</label>
                            <input type="text"
                                name="otp_code"
                                maxlength="4"
                                placeholder="Enter OTP"
                                class="font-size14 effect " />
                            <span class="focus-border"></span>
                        </div>

                        <div class="text-center mt-4 mb-4 btn-otp-div">
                            <button class="btn-common btn-otp "
                                    type="submit"> Submit
                            </button>
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center mt-2">
                                <a class="signup-btn-resend" >Resend </a>
                            </div>
                        </div>
                        </div>
                        <div class="form-group col-lg-12 col-sm-12 col-md-12 margin mt10mob otp-terms"
                            >
                            Note :
                            <ol start="1" class="list_ol">
                                <b>
                                <li id="email-id-li" class=" hide ">
                                        Please enter the OTP which has been sent to your Email Id / Mobile No

                                    </li>
                                    <li id="otp-mobile-li" class="">
                                        Please enter the OTP which has been sent to your Mobile No / Email Id

                                    </li>
                                    <li> In case, you don’t receive the OTP, kindly click on Resend button.</li>
                                    <li> No. of attempts for OTP Re-Generation is restricted to 3.</li>
                                </b>
                            </ol>
                        </div>

                    </form>
                </div>
    }
    render(){
        return (
            <>
                {this.renderContent()}
            </>
        )
    }
}
export default OtpComponent;
