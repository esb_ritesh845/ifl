import {PureComponent} from 'react';
import {connect} from 'react-redux';
import OtpComponent from './Otp.component';
// import PropTypes from 'prop-types';
const mapStateToProps = state => ({

})
const mapDispatchToProps = dispatch => ({

})

class OtpContainer extends PureComponent{

    render(){
        return (
            <>
                <OtpComponent />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(OtpContainer)
