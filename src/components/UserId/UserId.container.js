import {PureComponent} from 'react';
import {connect} from 'react-redux';
import UserIdComponent from './UserId.component';
// import PropTypes from 'prop-types';

const mapStateToProps = state => ({
})
const mapDispatchToProps = dispatch => ({
})

class UserIdContainer extends PureComponent{

    render(){
        return (
            <>
                <UserIdComponent  />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(UserIdContainer)
