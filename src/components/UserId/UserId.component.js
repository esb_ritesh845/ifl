import {PureComponent} from 'react';

class UserIdComponent extends PureComponent {
    renderContent(){
        return <div className="tab-pane" id="userIdContent" role="tabpanel">
                <div id="login_box" className="w60">
                    <span className="error-msg font-size10 red-color"></span>
                    <div className="row">
                        <form action="" method="post" id="loginForm" name="loginForm"  className="width100">
                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-40">
                                <div className="form-group">
                                    <label className="input-lbl m-font17">User ID</label>
                                    <input type="text" className="form-control" name="user_id" placeholder="Enter User ID"
                                        autoComplete="off" defaultValue="" />
                                </div>
                            </div>

                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-40">
                                <div className="form-group">
                                    <label className="input-lbl m-font17">Password</label>
                                    <input type="password" className="form-control" name="password" id="password" required
                                        placeholder="Enter Password" autoComplete="off" />
                                </div>
                            </div>

                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-40 text-center">

                                <button className="btn-common btn-login"  type="submit"> SUBMIT</button>
                            </div>

                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 mt-40 text-center">
                                <span className="forgot_btn" id="forgot_div"> Forgot Password?  </span>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
    }
    render(){
        return(
            <>
                {this.renderContent()}
            </>
        )
    }
}
export default UserIdComponent;
