import {PureComponent} from 'react';
import {connect} from 'react-redux';
import ApplyOnlineFormComponent from './ApplyOnlineForm.component';

import PropTypes from 'prop-types';
import {
        changeBasicInfoStatus,
        changeApplicantStatus,
        changeDocumentStatus,
        changePaymentStatus 
    } from '../../store/ApplyOnlineDetails/applyOnlineDetail.action';

const mapStateToProps = state => ({
    basicInfoStatus:state.ApplyOnlineDetailsReducer.basicInfoStatus,
    applicantStatus:state.ApplyOnlineDetailsReducer.applicantStatus,
    documentStatus : state.ApplyOnlineDetailsReducer.documentStatus,
    paymentStatus : state.ApplyOnlineDetailsReducer.paymentStatus
})

const mapDispatchToProps = dispatch => ({
    changeBasicInfoStatus: state => dispatch(changeBasicInfoStatus(state)),
    changeApplicantStatus: state => dispatch(changeApplicantStatus(state)),
    changeDocumentStatus: state => dispatch(changeDocumentStatus(state)),
    changePaymentStatus: state => dispatch(changePaymentStatus(state)),

})

class ApplyOnlineFormContainer extends PureComponent{

    static propTypes = {
        basicInfoStatus:PropTypes.bool.isRequired,
        applicantStatus:PropTypes.bool.isRequired,
        changeBasicInfoStatus:PropTypes.func.isRequired,
        changeApplicantStatus:PropTypes.func.isRequired
    }
    render(){
        return (
            <>
                <ApplyOnlineFormComponent {...this.props} />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ApplyOnlineFormContainer)
