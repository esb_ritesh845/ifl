import {PureComponent} from 'react';
import {Link} from  'react-router-dom';
import NavBarContainer from '../NavBar/NavBar.container';
import Footer from '../Footer';
class ApplyOnlineComponent extends PureComponent {
    renderNavBarComponent(){
        return <NavBarContainer />;
    }
    renderFooterComponent(){
        return <Footer />
    }
    renderContent(){
        return <div className="container-fluid main_content">
                    <div className="row">
                        <div className="col-lg-2 col-md-2 hidden-sm hidden-xs side_img"></div>
                        <div className="col-lg-10 col-md-10 col-sm-12 padding_main_box">
                            <div className="hid_titl"> Apply Online <br />
                                <span className="sub_titl_main" >
                                    Avail best interest rates on your home loan!
                                </span>
                            </div>
                            <span className="form-group mt-4">
                            <label className="font-size15 titlegrey">
                                What loan are you looking for?
                            </label>
                            </span>

                            <div className="card_inner padding0 mt20 mb60">
                                <div className="row mt-3">
                                    <div className="col-md-8 mob-mb-15">
                                        <div className="card card-apply" >
                                            <Link to="apply-online-form" className="card_text card-anchor">Home Loans / Home Improvement Loans / Home Extension Loans / Loan Against Property</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    }
    render(){
        return (
            <>
                {this.renderNavBarComponent()}
                {this.renderContent()}
                {this.renderFooterComponent()}
            </>
        )
    }
}
export default ApplyOnlineComponent;
