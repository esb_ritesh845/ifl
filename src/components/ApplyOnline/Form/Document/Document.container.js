import {PureComponent} from 'react';
import {connect} from 'react-redux';
import DocumentComponent from './Document.component';
import PropTypes from 'prop-types';
import {changePaymentStatus,changeApplicantStatus} from '../../../../store/ApplyOnlineDetails/applyOnlineDetail.action';

const mapStateToProps = state => ({
    documentStatus:state.ApplyOnlineDetailsReducer.documentStatus,
    isSalaried:state.ApplyOnlineDetailsReducer.documentStatus,
    isProfessional:state.ApplyOnlineDetailsReducer.documentStatus
})
const mapDispatchToProps = dispatch => ({
    changePaymentStatus: state => dispatch(changePaymentStatus(state)),
    changeApplicantStatus: state => dispatch(changeApplicantStatus(state)),


})

class DocumentContainer extends PureComponent{
    static propTypes = {
        documentStatus:PropTypes.bool.isRequired,
        changePaymentStatus:PropTypes.func.isRequired,
        changeApplicantStatus:PropTypes.func.isRequired,
        isSalaried:PropTypes.bool.isRequired,
        isProfessional:PropTypes.bool.isRequired,
    }
    render(){
        console.log(this.props)
        return (
            <>
                <DocumentComponent {...this.props} />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(DocumentContainer)
