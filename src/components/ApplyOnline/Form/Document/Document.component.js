import {PureComponent} from 'react';

class DocumentComponent extends PureComponent {

    renderSalarySlip(){
        const{isSalaried} = this.props

        if(!isSalaried){
            return null
        }

        return <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                    <label className="font-size15 titlegrey float-left">6 months salary Statement</label>
                    <input type="file" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autocomplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
    }

    renderSalaryCertificate(){
        const{isProfessional,isSalaried} = this.props

        if(isSalaried || isProfessional){
            return <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                        <label className="font-size15 titlegrey float-left">Salary Slip/Certificate/Income Proof</label>
                        <input type="file" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autocomplete="off" aria-invalid="false" />
                        <span className="focus-border"></span>
                    </div>
        }
        return null
        
        
    }
            
    
    renderContent(){

        const{
            changeApplicantStatus,
            changePaymentStatus,
        } = this.props;

        return <div className="applicant_card margintop10 applicant-tab fly-in visible">
                    <div className="sub_titl_main hid_titl"> Provide Loan Applicants Document</div>
                    <div className="card_header_name" >
                        <span className="mdi mdi-account"></span> Applicant: Ritesh
                    </div>
                    <div className="card-content">
                        <div className="row">
                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">Photo</label>
                                <input type="file" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autocomplete="off" aria-invalid="false" />
                                <span className="focus-border"></span>
                            </div>
                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">Aadhar Card</label>
                                <input type="file" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autocomplete="off" aria-invalid="false" />
                                <span className="focus-border"></span>
                            </div>
                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">PanCard</label>
                                <input type="file" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autocomplete="off" aria-invalid="false" />
                                <span className="focus-border"></span>
                            </div>
                            <div className="form-group col-md-6 col-lg-6"></div>

                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">Latest Electricity bill</label>
                                <input type="file" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autocomplete="off" aria-invalid="false" />
                                <span className="focus-border"></span>
                            </div>

                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">Front page of Bank Passbook</label>
                                <input type="file" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autocomplete="off" aria-invalid="false" />
                                <span className="focus-border"></span>
                            </div>

                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">Complete property papers</label>
                                <input type="file" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autocomplete="off" aria-invalid="false" />
                                <span className="focus-border"></span>
                            </div>

                           

                        </div>
                        <div className="row">     
                            {this.renderSalarySlip()}
                            {this.renderSalaryCertificate()}
                        </div>    

                        <div className="row">
                            <div className="col-lg-12 text-center mb-5 mt-5">
                                <button type="button" id="back-button" className="btn_ron border0" onClick={(() => changeApplicantStatus(true))}>Back</button>
                                <button type="button" className=" bg-blue btn_ron border0 " onClick={(() => changePaymentStatus(true))} id="continue-button" >Continue</button>
                            </div>
                        </div>
                    </div>
            </div>
    }

    render(){
        return (
            <>
                {this.renderContent()}

            </>
        )
    }
}
export default DocumentComponent;
