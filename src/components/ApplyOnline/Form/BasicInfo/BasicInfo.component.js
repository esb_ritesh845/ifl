import {PureComponent} from 'react';

class BasicInfoComponent extends PureComponent {
    renderContent(){
        const{changeApplicantStatus} = this.props;
        return <div className="margintop20 basic-information-tab fly-in visible" id="basic-information" >
                <div className="card-content">
                    <div className="row">
                        <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                            <label className="font-size15 titlegrey float-left">Loan Type</label>
                            <select name="type_of_loan" id="type_of_loan" className=" effect">
                                <option value="">Select Loan Type</option>
                                <option value="HOU">Home Loans</option>
                                <option value="IMP">Home Improvement Loans</option>
                                <option value="EXT">Home Extension Loans</option>
                                <option value="EQT">Loan Against Property</option>
                            </select>
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group col-lg-6 col-md-6 mb-2"></div>
                        <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                            <label className="font-size15 titlegrey float-left">
                                Applicant Name
                                    </label>
                            <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                            <label className="font-size15 titlegrey float-left">
                                Father Name
                                    </label>
                            <input type="text" placeholder="" className="font-size14 effect" name="applicant_father_name" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                            <label className="font-size15 titlegrey float-left">
                                Date Of Birth
                                    </label>
                            <input type="text" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group col-md-6 col-lg-6 "></div>
                        <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                            <label className="font-size15 titlegrey float-left">Property Home No & Bldg</label>
                            <input type="text" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                            <label className="font-size15 titlegrey float-left">Property Area</label>
                            <input type="text" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                            <label className="font-size15 titlegrey float-left">Propery Located State</label>
                            <select name="type_of_loan" id="type_of_loan" className=" effect">
                                <option value="">Select State</option>

                            </select>
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                            <label className="font-size15 titlegrey float-left">Property Located City</label>
                            <input type="text" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group col-lg-6 col-sm-12 col-md-6 margin mb-4" >
                            <label className="font-size15 titlegrey float-left" for="expected_loan_amount">
                                Property Cost / Estimate
                                    </label>
                            <span className="mdi mdi-currency-inr inr_icon"></span>
                            <input type="text" placeholder="" className="font-size14 height50 effect padding10 paddingleft30" name="expected_loan_amount" id="expected_loan_amount" maxlength="12" value="" autoComplete="off" />
                            <span className="focus-border"></span>
                        </div>
                        <div className="form-group col-lg-6 col-sm-12 col-md-6 margin mb-4" >
                            <label className="font-size15 titlegrey float-left" for="expected_loan_amount">
                                Expected Loan Amount
                                    </label>
                            <span className="mdi mdi-currency-inr inr_icon"></span>
                            <input type="text" placeholder="" className="font-size14 height50 effect padding10 paddingleft30" name="expected_loan_amount" id="expected_loan_amount" maxlength="12" value="" autoComplete="off" />
                            <span className="focus-border"></span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 text-center mb-5">
                            <button type="button" id="back-button" className="btn_ron border0">Back</button>
                            <button type="button" className=" bg-blue btn_ron border0 " id="continue-button" onClick={(() => changeApplicantStatus(true))}>Continue</button>
                        </div>
                    </div>
                </div>
            </div>

    }

    render(){
        return (
            <>
                {this.renderContent()}

            </>
        )
    }
}
export default BasicInfoComponent;
