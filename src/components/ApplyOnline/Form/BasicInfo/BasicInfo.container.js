import {PureComponent} from 'react';
import {connect} from 'react-redux';
import BasicInfoComponent from './BasicInfo.component';
import PropTypes from 'prop-types';
import {changeApplicantStatus } from '../../../../store/ApplyOnlineDetails/applyOnlineDetail.action';
const mapStateToProps = state => ({
    applicantStatus:state.ApplyOnlineDetailsReducer.applicantStatus,
})
const mapDispatchToProps = dispatch => ({
    changeApplicantStatus: state => dispatch(changeApplicantStatus(state)),

})

class BasicInfoContainer extends PureComponent{
    static propTypes = {
        applicantStatus:PropTypes.bool.isRequired,
        changeApplicantStatus:PropTypes.func.isRequired
    }
    render(){
        return (
            <>
                <BasicInfoComponent {...this.props} />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(BasicInfoContainer)
