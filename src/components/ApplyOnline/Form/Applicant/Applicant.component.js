import {PureComponent} from 'react';

class ApplicantComponent extends PureComponent {

    renderOccupSalriedContent(){
        const{occupSalStatus} = this.props;
        if(!occupSalStatus){
            return
        }
        return <div className="row" >
                    <div className="form-group col-md-6 col-lg-6 mb-2 occup-salried" >
                        <label className="font-size15 titlegrey float-left">Type of Employeer</label>
                        <select name="type_of_loan" id="type_of_loan" className=" effect">
                            <option value="">Select</option>
                            <option value="Private">Private</option>
                            <option value="Semi-Government">Semi-Government</option>
                            <option value="Government">Government</option>
                        </select>
                    </div>
                    <div className={"form-group col-md-6 col-lg-6 mb-2 occup-salaried "}>
                        <label className="font-size15 titlegrey float-left">Mode of Salary</label>
                        <select name="type_of_loan" id="type_of_loan" className=" effect">
                            <option value="">Select</option>
                            <option value="Cash">Cash</option>
                            <option value="Bank-Deposit">Bank Deposit</option>
                        </select>
                    </div>
                    {this.renderCommonContent()}
                </div>
    }
    renderCommonContent(){
        return  <>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Employeer Name </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Employeer Address </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Employeer Mobile Number </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Working Since </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Monthly Salary </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-md-6 col-lg-6 mb-2 occup-salried" >
                <label className="font-size15 titlegrey float-left">Total Employees in company</label>
                <select name="type_of_loan" id="type_of_loan" className=" effect">
                    <option value="">Select</option>
                    <option value="0-10">0-10</option>
                    <option value="10-50">10-50</option>
                    <option value="50-100">50-100</option>
                </select>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Colleague Name </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Colleague Mobile Number </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>

        </>
    }
    renderWorkerCommon(){

        return <>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Address Of The Site Currently Working </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Year Of Experience </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Daily Income </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Total Number Of Works Days In Month </label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Monthly Total Income</label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
        </>
    }

    renderOccupMasionContent(){
        const{occupMasionStatus} = this.props;
        if(!occupMasionStatus){
            return
        }
        return <div className="row">
            {this.renderWorkerCommon()}
        </div>
    }
    renderOccupDailyWeageContent(){
        const{occupDailyWageStatus} = this.props;
        if(!occupDailyWageStatus){
            return
        }
        return <div className="row">
                    <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">Nature of work</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
                <div className="form-group col-lg-6 col-md-6"></div>
                {this.renderWorkerCommon()}
        </div>
    }

    renderOccupDriverContent(){
        const{occupDriverStatus} = this.props;
        if(!occupDriverStatus){
            return
        }
        return <div className="row">
                <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">DL Number</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
                <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">Vechicle RC Number</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
                {this.renderCommonContent()}
        </div>
    }
    renderBussinessCommonContent(){
        return <>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Name Of The Shop</label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <div className="titlegrey mb-2">GSTIN</div>
                <div className="options">
                    <input className="float-left" name="gstin" type="radio" defaultValue="yes"/>
                    <label className="font-size15  float-left mr-3 ml-2">Yes</label>
                    <input className="float-left" name="gstin" type="radio" defaultValue="no" />
                    <label className="font-size15  float-left ml-2">No</label>
                </div>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <div className="titlegrey mb-2">Shop Owned or Rented</div>
                <div className="options">
                    <input className="float-left" name="owned" type="radio" defaultValue="yes"/>
                    <label className="font-size15  float-left mr-3 ml-2">Owned</label>
                    <input className="float-left" name="ranted" type="radio" defaultValue="no" />
                    <label className="font-size15  float-left ml-2">Rented</label>
                </div>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Monthly Sale</label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Monthly Other Expenditure</label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Any supplier Name or Reference</label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Any supplier Name or Reference</label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                <label className="font-size15 titlegrey float-left">Supplier Mobile Number</label>
                <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                <span className="focus-border"></span>
            </div>
        </>
    }
    renderBussinessPermanentContent(){
        const{occupBussPerStatus} = this.props;
        if(!occupBussPerStatus){
            return
        }
        return <div className="row">

            {this.renderBussinessCommonContent()}
        </div>
    }

    renderBussinessMovableContent(){
        const{occupBussMovStatus} = this.props;
        if(!occupBussMovStatus){
            return
        }
        return <div className="row">
                    <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                <label className="font-size15 titlegrey float-left">Type of Movable Asset</label>
                <select name="type_of_loan" id="type_of_loan" className=" effect">
                    <option value="">Select</option>
                    <option value="SA">Own Van</option>
                    <option value="MW">Own Mini</option>
                    <option value="MW">Truck</option>
                    <option value="MW">Own Thela</option>
                    <option value="MW">Daily Open Market</option>
                </select>
                <span className="focus-border"></span>
            </div>
            {this.renderBussinessCommonContent()}
        </div>
    }
    renderProfessionalContent(){
        const{occupProfStatus} = this.props;
        if(!occupProfStatus){
            return
        }
        return <div className="row">
                <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                    <label className="font-size15 titlegrey float-left">Type of Profession</label>
                    <select name="type_of_loan" id="type_of_loan" className=" effect">
                        <option value="">Select</option>
                    </select>
                    <span className="focus-border"></span>
                </div>
                <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">In the Profession since(year)</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
                <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">Highest Degree Obtained</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
                <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">Year of Degree Obtained</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
                <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">Year of Degree Obtained</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
                <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">Name of Degree</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
                <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                    <label className="font-size15 titlegrey float-left">Monthly Income</label>
                    <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" defaultValue="" autoComplete="off" aria-invalid="false" />
                    <span className="focus-border"></span>
                </div>
        </div>
    }

    renderCoApplicant(){
        const{coApplicantStatus,applicantCount} = this.props
        var indents = [];

        for(let appCount = 1 ; appCount <= applicantCount ; appCount++ ){

            indents.push(<div className={"row mb-2 " + (coApplicantStatus ? ' show' : ' hide')}>
                    <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                        <label className="font-size15 titlegrey float-left">
                            Co-Applicant Name
                                </label>
                        <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                        <span className="focus-border"></span>
                    </div>
                    <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                        <label className="font-size15 titlegrey float-left">
                            Co-Applicant Mobile Number
                                </label>
                        <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                        <span className="focus-border"></span>
                    </div>
                    <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                        <label className="font-size15 titlegrey float-left">RelationShip With Applicant</label>
                        <select name="type_of_loan" id="type_of_loan" className=" effect">
                            <option value="">Select </option>

                        </select>
                        <span className="focus-border"></span>
                    </div>
                    <div className="form-group col-lg-6 col-sm-12 col-md-6 margin applicant mb-2" >
                        <label className="font-size15 titlegrey float-left">
                            Address
                                </label>
                        <input type="text" placeholder="" className="font-size14 effect" name="applicant_name" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                        <span className="focus-border"></span>
                    </div>
                    <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                        <label className="font-size15 titlegrey float-left">Occupation</label>
                        <select name="type_of_loan" id="type_of_loan" className=" effect">
                            <option value="">Select Option</option>
                            <option value="Salaried">Salaried</option>
                            <option value="Mason Worker">Mason Worker</option>
                            <option value="Other Daily Wager Worker">Other Daily Wager Worker</option>
                            <option value="Heavy Vehicle Driver">Heavy Vehicle Driver</option>
                            <option value="LCV Driver">LCV Driver</option>
                            <option value="Business Permanent Setup">Business Permanent Setup</option>
                            <option value="Business Movable Setup">Business Movable Setup</option>
                            <option value="Professional">Professional</option>
                        </select>
                        <span className="focus-border"></span>
                    </div>
                    {this.renderOccupSalriedContent()}
                    <div className="col-md-12">
                         <hr />
                    </div>
                </div>

                )
        }
        return indents;
    }

    renderContent(){

        const{changeBasicInfoStatus,changeDocumentStatus,changeOccupation,coApplicantStatus,changeCoApplicantStatus,changeApplicantCount,applicantCount} = this.props;
        return <div className="applicant_card margintop10 applicant-tab fly-in visible">
                    <div className="sub_titl_main hid_titl"> Provide Loan Applicants Basic Information</div>
                    <div className="card_header_name" >
                        <span className="mdi mdi-account"></span> Applicant: Ritesh
                    </div>
                    <div className="card-content">
                        <div className="row">
                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">Currently Residing State</label>
                                <select name="type_of_loan" id="type_of_loan" className=" effect">
                                    <option value="">Select</option>

                                </select>
                                <span className="focus-border"></span>
                            </div>
                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">Currently Residing City</label>
                                <input type="text" placeholder="" className="font-size14 effect" name="applicant_dob" data-index="0" value="" autoComplete="off" aria-invalid="false" />
                                <span className="focus-border"></span>
                            </div>
                            <div className="form-group  col-lg-6 col-sm-12 col-md-6 margin mb-2" >
                                <label className="font-size15 titlegrey float-left">Occupation</label>
                                <select name="type_of_loan" id="type_of_loan" className=" effect" onChange={((event) => changeOccupation(event))}>
                                    <option value="">Select Option</option>
                                    <option value="SA">Salaried</option>
                                    <option value="MW">Mason Worker</option>
                                    <option value="DW">Other Daily Wager Worker</option>
                                    <option value="HD">Heavy Vehicle Driver</option>
                                    <option value="LD">LCV Driver</option>
                                    <option value="BP">Business Permanent Setup</option>
                                    <option value="BM">Business Movable Setup</option>
                                    <option value="PP">Professional</option>
                                </select>
                                <span className="focus-border"></span>
                            </div>
                        </div>
                        <div className="mt-3">
                            {this.renderOccupSalriedContent()}
                            {this.renderOccupMasionContent()}
                            {this.renderOccupDailyWeageContent()}
                            {this.renderOccupDriverContent()}
                            {this.renderBussinessPermanentContent()}
                            {this.renderBussinessMovableContent()}
                            {this.renderProfessionalContent()}
                        </div>
                        <div className="row">
                            <div className="form-group col-lg-6 col-sm-12 col-md-6 margin marginbottom10  mt-4" >
                                <div className="">
                                    <div className="cc-wrapcomp flexthree">
                                        <div className="titlegrey">
                                        Do You Wish To Add Co-Applicants While Applying For This Loan?</div>
                                        <div className="comp-subtitle font-size11 font-light lightgrey">
                                            ( All owners / proposed owners of the property have to be applicant to the loan. )
                                        </div>
                                    </div>
                                    <div class="options" >
                                            <input name="co_applicant" type="radio" id="co_applicant1" onChange={((event) => changeCoApplicantStatus(event))}  defaultValue="YES" />
                                            <label for="co_applicant1" className="co-operation-lable mr-3 ml-2">Yes</label>

                                            <input name="co_applicant" type="radio" id="co_applicant2" onChange={((event) => changeCoApplicantStatus(event))}  defaultValue="NO" defaultChecked="true" />
                                            <label for="co_applicant2" className="co-operation-lable ml-2" >No</label>
                                    </div>
                                </div>
                            </div>
                            <div className={"form-group col-lg-12 col-sm-12 col-md-12 marginbottom10 co-applicant mt-4" + (coApplicantStatus ? ' show' : ' hide')} >
                                <div className="cc-wrapcomp">
                                    <div className="titlegrey">
                                        No of Co-applicant's in addition to Applicant
                                    </div>
                                    <div className="comp-subtitle font-size13 font-light lightgrey">
                                            ( You can add a maximum of 3 Co-applicants )
                                        </div>
                                    </div>
                                    <span className="flex-display margintop10 count-no">
                                        <div className="tbbbtn-wrap tbbbtn-round">
                                            <input className="ttb co-applicant-count" id="ttb11" type="radio" name="co_applicant_count" defaultValue="1" onChange={((event) => changeApplicantCount(event))} className="hide" defaultChecked />
                                            <label className="ttb-btn m-0 btn height30 flex-display flex-center font-size16 reds" for="ttb11">1</label>
                                        </div>
                                        <div className="tbbbtn-wrap tbbbtn-round">
                                            <input className="ttb co-applicant-count" id="ttb12" type="radio" name="co_applicant_count" defaultValue="2" onChange={((event) => changeApplicantCount(event))} className="hide" />
                                            <label className="ttb-btn m-0 btn height30 flex-display flex-center font-size16 reds" for="ttb12">2</label>
                                        </div>
                                        <div className="tbbbtn-wrap tbbbtn-round">
                                            <input className="ttb co-applicant-count" id="ttb13" type="radio" name="co_applicant_count" defaultValue="3" onChange={((event) => changeApplicantCount(event))} className="hide" />
                                            <label className="ttb-btn m-0 btn height30 flex-display flex-center font-size16 reds" for="ttb13">3</label>
                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div className={"card_header_name mt-3" + (coApplicantStatus ? ' show' : ' hide')} >
                                <span className="mdi mdi-account"></span> Co-Applicant:
                            </div>

                            {this.renderCoApplicant()}

                            <div className="row">
                                <div className="col-lg-12 text-center mb-5 mt-5">
                                    <button type="button" id="back-button" className="btn_ron border0" onClick={(() => changeBasicInfoStatus(true))}>Back</button>
                                    <button type="button" className=" bg-blue btn_ron border0 " onClick={(() => changeDocumentStatus(true))} id="continue-button" >Continue</button>
                                </div>
                            </div>
                    </div>
            </div>
    }

    render(){
        return (
            <>
                {this.renderContent()}

            </>
        )
    }
}
export default ApplicantComponent;
