import {PureComponent} from 'react';
import {connect} from 'react-redux';
import ApplicantComponent from './Applicant.component';
import PropTypes from 'prop-types';
import {changeBasicInfoStatus,changeDocumentStatus } from '../../../../store/ApplyOnlineDetails/applyOnlineDetail.action';
const mapStateToProps = state => ({
    basicInfoStatus:state.ApplyOnlineDetailsReducer.applicantStatus,
})
const mapDispatchToProps = dispatch => ({
    changeBasicInfoStatus: state => dispatch(changeBasicInfoStatus(state)),
    changeDocumentStatus: state => dispatch(changeDocumentStatus(state)),


})

class ApplicantContainer extends PureComponent{
    static propTypes = {
        basicInfoStatus:PropTypes.bool.isRequired,
        changeBasicInfoStatus:PropTypes.func.isRequired,
        changeDocumentStatus:PropTypes.func.isRequired,
    }
    state = {
        occupSalStatus:false,
        occupMasionStatus:false,
        occupDailyWageStatus:false,
        occupDriverStatus:false,
        occupBussPerStatus:false,
        occupBussMovStatus:false,
        occupProfStatus:false,
        coApplicantStatus:false,
        applicantCount:1
    }


    changeOccupation(event){
        let status = event.target.value;
        if(status === 'SA'){
            this.setState({
                occupSalStatus:true,
                occupDailyWageStatus:false,
                occupDriverStatus:false,
                occupMasionStatus:false,
                occupBussPerStatus:false,
                occupBussMovStatus:false,
                occupProfStatus:false
            });
        }else if(status === 'MW'){
            this.setState({
                occupMasionStatus:true,
                occupSalStatus:false,
                occupDriverStatus:false,
                occupDailyWageStatus:false,
                occupBussPerStatus:false,
                occupBussMovStatus:false,
                occupProfStatus:false
            })
        }else if(status === 'DW'){
            this.setState({
                occupDailyWageStatus:true,
                occupMasionStatus:false,
                occupSalStatus:false,
                occupDriverStatus:false,
                occupBussPerStatus:false,
                occupBussMovStatus:false,
                occupProfStatus:false
            })
        }else if(status === 'HD' || status === 'LD'){
            this.setState({
                occupDriverStatus:true,
                occupDailyWageStatus:false,
                occupMasionStatus:false,
                occupSalStatus:false,
                occupBussPerStatus:false,
                occupBussMovStatus:false,
                occupProfStatus:false
            })
        }else if(status === 'BP'){
            this.setState({
                occupBussPerStatus:true,
                occupDriverStatus:false,
                occupDailyWageStatus:false,
                occupMasionStatus:false,
                occupSalStatus:false,
                occupBussMovStatus:false,
                occupProfStatus:false
            })
        }else if(status === 'BM'){
            this.setState({
                occupBussPerStatus:false,
                occupDriverStatus:false,
                occupDailyWageStatus:false,
                occupMasionStatus:false,
                occupSalStatus:false,
                occupBussMovStatus:true,
                occupProfStatus:false
            })
        }else if(status === 'PP'){
            this.setState({
                occupBussPerStatus:false,
                occupDriverStatus:false,
                occupDailyWageStatus:false,
                occupMasionStatus:false,
                occupSalStatus:false,
                occupBussMovStatus:false,
                occupProfStatus:true
            })
        }else{
            this.setState({occupSalStatus:false,
                occupMasionStatus:false,
                occupDailyWageStatus:false,
                occupDriverStatus:false,
                occupBussPerStatus:false,
                occupBussMovStatus:false,
                occupProfStatus:false
            });
        }
    }
    changeCoApplicantStatus(event){
        let value = event.target.value;
        if(value == 'YES'){
            this.setState({coApplicantStatus:true});
        }else{
            this.setState({coApplicantStatus:false});
        }
    }

    changeApplicantCount(event){
        let value = event.target.value;
        // console.log(value);
        this.setState({applicantCount:value})

    }

    render(){
        return (
            <>
                <ApplicantComponent
                {...this.props}
                {...this.state}
                changeOccupation = {this.changeOccupation.bind(this)}
                changeCoApplicantStatus = {this.changeCoApplicantStatus.bind(this)}
                changeApplicantCount = {this.changeApplicantCount.bind(this)}

                 />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ApplicantContainer)
