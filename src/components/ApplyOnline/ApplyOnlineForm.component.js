import {PureComponent} from 'react';
import Footer from '../Footer';
import BasicInfoForm from './Form/BasicInfo/BasicInfo.container'
import ApplicantForm from './Form/Applicant/Applicant.container'
import DocumentForm from './Form/Document/Document.container'
import NavBarContainer from '../NavBar/NavBar.container';

class ApplyOnlineFormComponent extends PureComponent {
    renderNavBarComponent(){
        return <NavBarContainer />;
    }
    renderFooterComponent(){
        return <Footer />
    }

    renderContent(){
        const{
            basicInfoStatus,
            applicantStatus,
            documentStatus,
            paymentStatus
        } = this.props;

        return <div className="container-fluid main_content">
                    <div className="row">
                        <div className="col-lg-2 col-md-2 hidden-sm hidden-xs side_img"></div>
                        <div className="col-lg-10 col-md-10 col-sm-12 padding_main_box">
                            <div className="hid_titl"> Apply Online <br />
                                <span className="sub_titl_main" >
                                    Avail best interest rates on your home loan!
                                </span>
                            </div>
                            <span className="form-group mt-4">
                            <label className="font-size15 titlegrey">
                                What loan are you looking for?
                            </label>
                            </span>
                            <div className="card_inner padding0 mt20 mb60 mt-3 mb-5">
                                <div className="elig-tabs flex-display">
                                    <div className={"et-tabs font-size16 flex-display  form-step " + (basicInfoStatus ? 'active' : '') } data-index="1">
                                        <div className="margin hand"> <span className="round_count"> 1 </span> Basic Information </div>
                                    </div>
                                    <div className={"et-tabs font-size16 flex-display  form-step " + (applicantStatus ? 'active' : '') } data-index="2">
                                        <div className="margin hand"> <span className="round_count"> 2 </span> Applicants </div>
                                    </div>
                                    <div className={"et-tabs font-size16 flex-display  form-step " + (documentStatus ? 'active' : '') } data-index="3">
                                        <div className="margin hand"> <span className="round_count"> 3 </span> Documents </div>
                                    </div>
                                    <div className={"et-tabs font-size16 flex-display  form-step " + (paymentStatus ? 'active' : '') } data-index="3">
                                        <div className="margin hand"> <span className="round_count"> 3 </span> Payment </div>
                                    </div>
                                </div>
                                <div className="container-fluid marginbottom20">
                                    <form action="" method="post" className="full eligibility-form" id="eligibility-form" novalidate="novalidate">
                                        {basicInfoStatus ? <BasicInfoForm /> :'' }
                                        {applicantStatus ?<ApplicantForm /> : ''}
                                        {documentStatus ? <DocumentForm /> : ''}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    }
    render(){
        return (
            <>
                {this.renderNavBarComponent()}
                {this.renderContent()}
                {this.renderFooterComponent()}
            </>
        )
    }
}
export default ApplyOnlineFormComponent;
