import {PureComponent} from 'react';
import {connect} from 'react-redux';
import ApplyOnlineComponent from './ApplyOnline.component';
// import PropTypes from 'prop-types';
const mapStateToProps = state => ({

})
const mapDispatchToProps = dispatch => ({

})

class ApplyOnlineContainer extends PureComponent{

    render(){
        return (
            <>
                <ApplyOnlineComponent />
            </>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ApplyOnlineContainer)
