import {PureComponent} from 'react';

class NavBar extends PureComponent {
    renderContent(){
        return <nav className="navbar navbar-light top-head-fix">
                <div className="container-fluid">
                    <div className="be-navbar-header"><a className="top-logo-head" href="/"></a></div>
                    <div className="be-right-navbar">
                        <div className="round_icon col-md-1 pull-right">
                            <a href="" target="_blank" rel="noopener noreferrer">
                                <img src="images/phone-icon.png" alt="www.ifl.com" width="35px;" />
                            </a>
                        </div>
                    </div>
                </div>
            </nav>
    }

    render(){
        return (
            <>
                {this.renderContent()}
            </>
        );
    }
  }

  export default NavBar;
