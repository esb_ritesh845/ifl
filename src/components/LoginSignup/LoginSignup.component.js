import {PureComponent} from 'react';
import SignupContainer from '../Signup/Signup.container';
import UserIdContainer from '../UserId/UserId.container';
import OtpContainer from '../Otp/Otp.container';
import Footer from '../Footer';
import NavBarContainer from '../NavBar/NavBar.container';
class LoginSignup extends PureComponent {

    renderNavBarComponent(){

        return <NavBarContainer />;
    }
    renderFooterComponent(){
        return <Footer />;
    }
    renderContent(){
        const {signupStatus,userIdStatus,otpStatus,changeSignupStatus,changeUserIdStatus} = this.props;
        return   <div className="container-fluid bg-white singup_bg">
                    <div className="signup_card">
                        <div className="row margin0">
                            <div className="col-lg-8">
                                <div className="signup_box">
                                    <div className="login-head"> Login Or SignUp</div>
                                    <div className="signup_sub_text"> Kindly sign up if you are a new user or login with your existing credentials.
                                    </div>
                                </div>

                                <div className="row signup-tab tab-signup-div">
                                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                                        <li className="nav-item">
                                            <a className={"nav-link " + (signupStatus ? 'active' : '')}  id="withaccount" data-toggle="tab" role="tab"
                                            aria-controls="home" >
                                                <div><input id="radio-1" className="radio-custom" name="radio-group" type="radio"
                                                            defaultChecked={signupStatus}
                                                        onChange={(() => changeSignupStatus(true))}
                                                            />
                                                    <label htmlFor="radio-1" className="radio-custom-label"> </label></div>
                                                Sign Up
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a className={"nav-link " + (userIdStatus ? 'active' : '')} id="withlogin" data-toggle="tab" role="tab"
                                            aria-controls="profile" >
                                                <div><input id="radio-2" className="radio-custom" name="radio-group" type="radio" defaultChecked={userIdStatus} onChange={(() => changeUserIdStatus(true))} />
                                                    <label htmlFor="radio-2" className="radio-custom-label"> </label></div>
                                                User ID
                                            </a>
                                        </li>

                                    </ul>
                                    <div className="tab-content">
                                          {signupStatus ? <SignupContainer /> : '' }
                                          {userIdStatus ? <UserIdContainer  /> : '' }
                                          {otpStatus ? <OtpContainer /> : ''}

                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4"><img className="signup_img" src="../images/signup_pic.png" /></div>
                        </div>
                    </div>
                </div>
    }
    render(){
        return (
            <>
                {this.renderNavBarComponent()}
                {this.renderContent()}
                {this.renderFooterComponent()}
            </>
        )
    }
}

export default LoginSignup;
