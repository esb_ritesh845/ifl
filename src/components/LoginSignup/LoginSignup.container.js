import {PureComponent} from 'react';
import {connect} from 'react-redux';
import LoginSignupComponent from './LoginSignup.component';
import PropTypes from 'prop-types';
import {changeOtpStatus, changeSignupStatus, changeUserIdStatus} from '../../store/loginDetails/loginDetail.action'
const mapStateToProps = state => ({
    signupStatus:state.LoginDetailsReducer.signupStatus,
    userIdStatus:state.LoginDetailsReducer.userIdStatus,
    otpStatus:state.LoginDetailsReducer.otpStatus
})
const mapDispatchToProps = dispatch => ({
    changeSignupStatus: state => dispatch(changeSignupStatus(state)),
    changeUserIdStatus: state => dispatch(changeUserIdStatus(state))
})

class LoginSignupContainer extends PureComponent{
    static propTypes = {
        signupStatus:PropTypes.bool.isRequired,
        changeSignupStatus:PropTypes.func.isRequired
    }
    render(){
        return(
            <>
                <LoginSignupComponent {...this.props} />
            </>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(LoginSignupContainer)
