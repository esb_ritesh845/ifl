import {
    BASICINFO_STATUS,
    APPLICANT_STATUS,
    DOCUMENT_STATUS,
    PAYMENT_STATUS
}  from './applyOnlineDetail.action'

export const initialState = {
    basicInfoStatus : true,
    applicantStatus : false,
    documentStatus : false,
    paymentStatus : false,
    isProfessional:false,
    isSalaried:false
}

export const ApplyOnlineDetailsReducer = (
    state = initialState,
    action
) => {

    switch (action.type) {
        case BASICINFO_STATUS:
            return {
                    ...state,
                    basicInfoStatus:action.status,
                    applicantStatus:false,
                    documentStatus : false,
                    paymentStatus : false
                };

        case APPLICANT_STATUS:
            return {
                    ...state,
                    applicantStatus:action.status,
                    basicInfoStatus:false,
                    documentStatus : false,
                    paymentStatus : false
                }

        case DOCUMENT_STATUS:
            return {
                    ...state,
                    documentStatus : action.status,
                    applicantStatus:false,
                    basicInfoStatus:false,
                    paymentStatus : false
                }

        case PAYMENT_STATUS:
            return {
                    ...state,
                    paymentStatus:action.status,
                    basicInfoStatus:false,
                    documentStatus : false,
                    applicantStatus : false
                }

        default:
            return state
    }
}

export default ApplyOnlineDetailsReducer;
