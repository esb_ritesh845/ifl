
export const BASICINFO_STATUS = "BASICINFO_STATUS";
export const APPLICANT_STATUS = "APPLICANT_STATUS";
export const DOCUMENT_STATUS  = "DOCUMENT_STATUS";
export const PAYMENT_STATUS   = "PAYMENT_STATUS";

export const changeBasicInfoStatus = (status) =>{
    return {
        type:BASICINFO_STATUS,
        status
    }
}
export const changeApplicantStatus = (status) =>{
    return {
        type:APPLICANT_STATUS,
        status
    }
}

export const changeDocumentStatus = (status) =>{
    return {
        type:DOCUMENT_STATUS,
        status
    }
}
export const changePaymentStatus = (status) =>{
    return {
        type:PAYMENT_STATUS,
        status
    }
}