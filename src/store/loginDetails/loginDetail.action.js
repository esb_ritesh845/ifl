export const SAVE_TOKEN = "SAVE_TOKEN"
export const SIGNUP_STATUS = "SIGNUP_STATUS";
export const USERID_STATUS = "USERID_STATUS";
export const OTP_STATUS = "OTP_STATUS";

export const saveToken =(status)=>{
    return {
        type:SAVE_TOKEN,
        status
    }
}

export const changeSignupStatus = (status) =>{
    return {
        type:SIGNUP_STATUS,
        status
    }
}
export const changeUserIdStatus = (status) =>{
    return {
        type:USERID_STATUS,
        status
    }
}
export const changeOtpStatus = (status) =>{
    return {
        type:OTP_STATUS,
        status
    }
}

