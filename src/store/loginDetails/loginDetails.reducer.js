import {
    SAVE_TOKEN, SIGNUP_STATUS,USERID_STATUS,OTP_STATUS
}  from './loginDetail.action'

export const initialState = {
    token:'',
    signupStatus : true,
    userIdStatus : false,
    otpStatus : false,
    userDetails:{
        firstName : 'Ritesh',
        lastName : 'Panchal'
    }
}

export const LoginDetailsReducer = (
    state = initialState,
    action
) => {

    switch (action.type) {
        case SAVE_TOKEN:
            console.log(action)
            return {...state ,token:action.status};
        case SIGNUP_STATUS:
            return {
                    ...state,
                    signupStatus:action.status,
                    userIdStatus:false,
                    otpStatus:false
                }
        case USERID_STATUS:
            return {
                ...state,
                    userIdStatus:action.status,
                    signupStatus:false,
                    otpStatus:false
                }
        case OTP_STATUS:
            return {
                ...state,
                otpStatus:action.status,
                signupStatus:false,
                userIdStatus:false
            }
        default:
            return state
    }
}

export default LoginDetailsReducer;
